import ProductItem from "./ProductItem";
import classes from "./Products.module.css";

const DUMMY_PRODUCTS = [
  {
    id: "p1",
    price: 6,
    title: "harry potter 1",
    description: "harry potter and the philosopher's stone",
  },
  {
    id: "p2",
    price: 10,
    title: "harry potter 2",
    description: "harry potter and the chamber of secrets",
  },
  {
    id: "p3",
    price: 14,
    title: "harry potter 3",
    description: "harry potter ans the prisoner of azkaban",
  },
  {
    id: "p4",
    price: 30,
    title: "harry potter 4",
    description: "harry potter and the goblet of fire",
  },
  {
    id: "p5",
    price: 35,
    title: "harry potter 5",
    description: "harry potter and the order of the pheonix",
  },
  {
    id: "p6",
    price: 40,
    title: "harry potter 6",
    description: "harry potter and the half blood prince",
  },
  {
    id: "p7",
    price: 50,
    title: "harry potter 7",
    description: "harry potter and the deathly hallows part 1",
  },
  {
    id: "p8",
    price: 50,
    title: "harry potter 8",
    description: "harry potter and the deathly hallows part 2",
  },
];
const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_PRODUCTS.map((product) => (
          <ProductItem
            key={product.id}
            id={product.id}
            title={product.title}
            price={product.price}
            description={product.description}
          />
        ))}
      </ul>
    </section>
  );
};

export default Products;
