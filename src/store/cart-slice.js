import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  items: [],
  totalQuantity: 0,
  changed:false
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    replaceCart(state, action) {
      state.totalQuantity = action.payload.totalQuantity;
      state.items = action.payload.items;
    },
    addItem(state, action) {
      const item = action.payload;
      const existingItem = state.items.find(
        (someitem) => someitem.id === item.id
      );
      if (!existingItem) {
        state.items.push({
          id: item.id,
          price: item.price,
          quantity: 1,
          item,
          totalPrice: item.price,
          name: item.title,
        });
      } else {
        existingItem.quantity++;
        existingItem.totalPrice += item.price;
      }
      state.changed=true;
      state.totalQuantity++;
    },
    removeItem(state, action) {
      const id = action.payload;
      const existingItem = state.items.find((someitem) => someitem.id === id);
      state.totalQuantity--;
      state.changed=true;
      if (existingItem.quantity === 1) {
        state.items = state.items.filter((someitem) => someitem.id !== id);
      } else {
        existingItem.quantity--;
        existingItem.totalPrice -= existingItem.price;
      }
      
    },
  },
});

export const cartActions = cartSlice.actions;
export default cartSlice;
